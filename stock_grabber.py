"""
Part of the beautiful swan project

Primary author: Russell Kaehler

This is the script used to pull each stock for full pull or update
It uses the alpha vantage api

(c) 2021
"""


import argparse
import collections
import datetime
import json
import os
import random
import socketserver
import sys
import time
import http.server
from urllib.parse import urlparse
from urllib.parse import parse_qs
import requests




CWD = os.path.dirname(os.path.realpath(__file__))
TOLERANCE_LIMIT = 5

def convert_dict(data):
    if isinstance(data, str):
        return str(data)
    elif isinstance(data, collections.abc.Mapping):
        return dict(map(convert_dict, data.items()))
    elif isinstance(data, collections.abc.Iterable):
        return type(data)(map(convert_dict, data))
    else:
        return data


def data_parser(path):
    my_data = []
    with open( path, 'r') as my_file:
        ini_data = json.load(my_file)

        for x in sorted(ini_data.keys()):
            daily_record = convert_dict(ini_data[x].copy())

            my_data.append(daily_record.copy())

    return my_data[:]


def dump_json_data_to_path(data, output_path):
    with open( output_path, "w") as output_file:
        json.dump(data, output_file, sort_keys=True, indent=2)


def generate_url(symbol, update=False):

    api_keys = ["3JFD95HIITPHAQRT", "KGST"]
    selected_key = random.choice(api_keys)
    url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=%s" % (symbol)
    if not update:
        url += "&outputsize=full&apikey=%s" % (selected_key)
    elif update:
        # url += "&outputsize=full&apikey=%s" % (selected_key)
        url += "&outputsize=compact&apikey=%s" % (selected_key)

    return url


def pull_symbol_from_site( symbol, update=False, attempt_count=0):
    url = generate_url(symbol, update)
    print(url)
    # time.sleep(random.random())
    response_code = -1
    check_count = 0
    initial_response = {}
    sleep_timer = 0.5 + random.random()
    while response_code != 200:
        time.sleep(sleep_timer)
        try:
            response = requests.get(url)
            initial_response = json.loads(response.text)
            response_code = int(response.status_code)
            if initial_response['Meta Data']["2. Symbol"] != symbol:
                if 'Time Series (Daily)' not in initial_response.keys():
                    response_code = -1
                    check_count += 1
        except:
            sleep_timer = (sleep_timer + random.random()) * 2
            time.sleep(sleep_timer)
            check_count += 1
        if check_count > TOLERANCE_LIMIT:
            print("hit error \t",check_count)
            print(symbol)
            print(url)

    # In progress 
    # cleaned_initial_response = clean_initial_response(initial_response)
    initial_response = test_initial_response(initial_response.copy(), symbol, update, attempt_count)

    if not initial_response:
        return False

    return convert_dict(initial_response).copy()

def test_initial_response(current_response, symbol, update, attempt_count):

    done = False
    if attempt_count > TOLERANCE_LIMIT:
        return False

    while not done:
        try:
            if not current_response:
                return False
            raw_response = current_response['Time Series (Daily)']
            done = True
        except KeyError:
            print("failed on \t", symbol)
            print(current_response)
            if "Error Message" in current_response.keys():
                print("found error message")
                if "Invalid API call" in current_response["Error Message"]:
                    attempt_count += 3
                attempt_count += 1
                print(attempt_count)
                if attempt_count > TOLERANCE_LIMIT:
                    print(attempt_count)
                    done = True
                    return False
            time.sleep(10)
            current_response = pull_symbol_from_site(symbol, update, attempt_count= attempt_count)

    return current_response.copy()

def clean_initial_response(initial_response):
    """ take response and make sure that it has the correct key format for dates"""
    raw_response = initial_response['Time Series (Daily)']
    final_response = {}

    key_list = list(sorted(raw_response.keys()))
    for key_index in xrange(len(key_list)):
        try:
            datetime.datetime.strptime(key_list[key_index], '%Y-%m-%d')
        except ValueError:
            problem_key = key_list[key_index]
            munged_key = problem_key.split()[0]
            key_list[key_index] = munged_key[:]

    for key_index in xrange(len(key_list)):
        key = key_list[key_index]
        try:
            record = raw_response[key]
            final_response[key_index] = record.copy()
        except:
            print("hit error in cleaning keys")
    return final_response.copy()


def compare_existing_records(current_response, output_file):

    #check that output file exists
    if os.path.exists(output_file):
        old_data = data_parser(output_file)
    else:
        print("this file is missing")
        # sys.exit(1)

    old_data_len = len(old_data)
    organized_keys = sorted(current_response.keys())
    record_keys = old_data[0].keys()
    organized_keys_len = len(organized_keys)

    #get last record from old
    final_date_record_old = old_data[-1]["date"]

    #get first record from new
    first_record_new = current_response[organized_keys[0]]["date"]
    # print "first and last of organized keys"
    first_date_update_pull = organized_keys[0]
    last_date_update_pull = organized_keys[-1]

    if first_date_update_pull > final_date_record_old:
        #gap in date, do new pull of full historic records
        #~then check all that co-occur and for rest add to set
        symbol = output_file[output_file.rfind("/")+1:output_file.find(".")]

        full_data_new_pull = pull_symbol_from_site(symbol, update=False)
    #if last date new < last date old && first date new < last date old
    if first_date_update_pull < final_date_record_old:

        compare_index = -1
        for record_index in xrange(old_data_len):
            record = old_data[record_index]
            temp_date = record["date"]
            if temp_date == first_date_update_pull:
                compare_index = record_index
                break
            if temp_date > first_date_update_pull:
                compare_index = record_index - 1
                break

        for date_index in xrange(organized_keys_len):
            compare_record_index = compare_index + date_index

            current_new_record = current_response[organized_keys[date_index]].copy()

            if compare_record_index < old_data_len:

                compare_record = old_data[compare_record_index]

                for daily_key in record_keys:
                    if compare_record[daily_key] != current_new_record[daily_key]:
                        #need better fix for this
                        old_data[compare_record_index] = current_new_record.copy()

            else:
                print("updating old")
                print(organized_keys[date_index])
                print(old_data_len,"\t",len(old_data))
                old_data.append(current_new_record.copy())

    #~~~ find date old in new
    #~~~ if values different, update
    #~~~ else start to append to output_file new data

    final_response = {}
    for i in old_data:
        final_response[i["date"]] = i.copy()

    dump_json_data_to_path(final_response, output_file)

    return 0



def pull_history(my_input):
    symbol, update = my_input[0], my_input[1]

    initial_response = pull_symbol_from_site(symbol, update)

    if not initial_response:
        return False

    prev_close = None
    prev_day = None

    error_count = 0
    try:
        raw_response = initial_response['Time Series (Daily)']
    except ValueError:
        print("hit error with symbol\t", symbol,"\t",error_count,"\ttimes(s)")
        initial_response = pull_symbol_from_site(symbol, update)

    final_response = {}
    key_list = list(sorted(raw_response.keys()))
    # print key_list
    for key_index in range(len(key_list)):
        try:
            datetime.datetime.strptime(key_list[key_index], '%Y-%m-%d')
        except ValueError:
            # print key_list[key_index]
            problem_key = key_list[key_index]
            munged_key = problem_key.split()[0]
            key_list[key_index] = munged_key[:]

    for day_index in range(len(key_list)):
        day_ini_format = key_list[day_index].split()[0]
        day_formatted = day_ini_format.replace("-", "")
        if day_index > 0:
            prev_day = key_list[day_index - 1].replace("-", "")
        final_response[day_formatted] = {}

        final_response[day_formatted]["open"] = float(raw_response[day_ini_format]["1. open"])
        final_response[day_formatted]["high"] = float(raw_response[day_ini_format]["2. high"])
        final_response[day_formatted]["low"] = float(raw_response[day_ini_format]["3. low"])
        final_response[day_formatted]["close"] = float(raw_response[day_ini_format]["4. close"])
        final_response[day_formatted]["adjClose"] = float(raw_response[day_ini_format]["5. adjusted close"])
        final_response[day_formatted]["volume"] = float(raw_response[day_ini_format]["6. volume"])
        final_response[day_formatted]["dividend"] = float(raw_response[day_ini_format]["7. dividend amount"])
        final_response[day_formatted]["split"] = float(raw_response[day_ini_format]["8. split coefficient"])
        final_response[day_formatted]["date"] = str(day_formatted)
        current_close = float(final_response[day_formatted]["close"])

        if prev_close is not None:

            if prev_close < current_close:
                final_response[prev_day]["class"] = 0
            else:
                final_response[prev_day]["class"] = 6
        else:
            final_response[day_formatted]["class"] = 7

        sma_size = 90
        if day_index > sma_size:
            sma_sum = 0.0
            last_ten_days = key_list[day_index-sma_size: day_index]
            for elt in last_ten_days:
                sma_sum += float(final_response[elt.replace("-", "")]["close"])
            sma_sum = sma_sum / (sma_size * 1.0)
            # print "my sma\t", sma_sum
            final_response[day_formatted]["sma_sum"] = round(sma_sum,2)

        prev_close = current_close

    if prev_day is not None:
        final_response[day_formatted]["class"] = 7

    return final_response


class MyHttpRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        # Sending an '200 OK' response
        self.send_response(200)

        # Setting the header
        self.send_header("Content-type", "application/json")

        # Whenever using 'send_header', you also have to call 'end_headers'
        self.end_headers()

        # Extract query param
        symbol = ''
        query_components = parse_qs(urlparse(self.path).query)
        if 'symbol' in query_components:
            symbol = query_components["symbol"][0]

        history = pull_history([symbol, False])

        read_history = json.dumps(history).encode('utf-8')
        
        self.wfile.write(read_history)
        return

def run_server():
    handler_object = MyHttpRequestHandler

    PORT = 9999
    my_server = socketserver.TCPServer(("", PORT), handler_object)

    # Star the server
    my_server.serve_forever()
    


if __name__ == '__main__':
    run_server()

